import os
import pillow_heif
from PIL import Image

pillow_heif.register_heif_opener()

def convert_heic_to_jpeg(heic_path, jpeg_path):
    """
    Converts a HEIC file to a JPEG file while attempting to preserve metadata and color profile.
    :param heic_path: Path to the HEIC file.
    :param jpeg_path: Path to the output JPEG file.
    """
    with Image.open(heic_path) as image:
        exif_data = image.info.get('exif', None)
        icc_profile = image.info.get('icc_profile', None)
        
        os.makedirs(os.path.dirname(jpeg_path), exist_ok=True)

        image.save(jpeg_path, "JPEG", quality=95, exif=exif_data, icc_profile=icc_profile)



def convert_heic_to_png(heic_path, png_path):
    heif_file = pyheif.read(heic_path)
    image = Image.frombytes(
        heif_file.mode,
        heif_file.size,
        heif_file.data,
        "raw",
        heif_file.mode,
        heif_file.stride,
    )

    if heif_file.color_profile:
        if isinstance(heif_file.color_profile, dict) and 'data' in heif_file.color_profile:
            image.info['icc_profile'] = heif_file.color_profile['data']
        elif isinstance(heif_file.color_profile, bytes):
            image.info['icc_profile'] = heif_file.color_profile

    os.makedirs(os.path.dirname(png_path), exist_ok=True)
    image.save(png_path, "PNG")


def find_and_convert_heic_files(source_folder):
    """
    Find all HEIC files in source_folder and convert them to JPEG.

    :param source_folder: Path to the source folder with HEIC files.
    """
    output_folder = os.path.join(source_folder, 'converted_photo')

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    i = 0
    for subdir, _, files in os.walk(source_folder):
        for file in files:
            if file.lower().endswith('.heic'):
                heic_file_path = os.path.join(subdir, file)
                base_name = os.path.splitext(os.path.basename(heic_file_path))[0]
                jpeg_file_path = os.path.join(output_folder, f"{base_name}.jpeg")

                print(f"try to convert to JPEG file - {base_name}")

                convert_heic_to_jpeg(heic_file_path, jpeg_file_path)
                # png_file_path = os.path.join(output_folder, f"{base_name}.png")
                # print(f"try to convert to PNG file - {base_name}")
                # convert_heic_to_png(heic_file_path, png_file_path)
                print(f"Successfully converted to JPEG file - {base_name}\n")
                i += 1
    print(f"Successfully converted to JPEG {i} files")


if __name__ == "__main__":
    source_folder = os.getcwd()
    find_and_convert_heic_files(source_folder)
